package ru.mish.recycler;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        RecyclerView messagesList = findViewById(R.id.messages_list);
        messagesList.setLayoutManager(new LinearLayoutManager(this));
        final MessagesAdapter adapter = new MessagesAdapter();
        messagesList.setAdapter(adapter);

        Button addButton = findViewById(R.id.add_button);
        final EditText messageEdit = findViewById(R.id.message_edit);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = messageEdit.getText().toString();
                adapter.addMessage(message);
            }
        });


    }

}
