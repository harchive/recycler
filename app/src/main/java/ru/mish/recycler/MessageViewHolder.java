package ru.mish.recycler;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MessageViewHolder extends RecyclerView.ViewHolder {

    private TextView messageText;

    public static MessageViewHolder create(Context context) {
        View view = View.inflate(context, R.layout.row_message, null);
        return new MessageViewHolder(view);
    }

    public MessageViewHolder(View view) {
        super(view);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        messageText = view.findViewById(R.id.message_text);
    }

    public void bindMessage(String message) {
        messageText.setText(message);
    }

}
